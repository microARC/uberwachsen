var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const mqtt = require('mqtt');
const opt = {
  port: 1883,
  clientId: 'uberwachsen'
}
// connect to broker
var client = mqtt.connect('tcp://127.0.0.1', opt);
client.on('connect', function () {
  console.log('connected to mosquitto');
  client.subscribe('autogrow/sensor');
  client.subscribe('autogrow/image');
});

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);

// socket io callbacks
io.on('connect', function (socket) {
  // callbacks from mqtt
  client.on('message', function (topic, msg) {
    // console.log('topic: '+ topic);
    if (topic === 'autogrow/sensor') {
      socket.emit('mqtt', msg.toString());
    } else if (topic === 'autogrow/image') {
      socket.emit('shot', msg.toString());
      // console.log(msg.toString());
    }
  });

  socket.on('command', function (data) {
    // console.log(data);
    client.publish('autogrow/cmd', JSON.stringify(data));
  });
});


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

module.exports = {app, server};
