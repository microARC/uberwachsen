import Vue from 'vue'
import Router from 'vue-router'
import Dash from '@/components/Dash'

// import DataCard from '@/components/DataCard'
// import Monitor from '@/components/Monitor'
// import Settings from '@/components/Settings'

Vue.use(Router)

export default new Router({
  routes: [
    // {
    //   path: '/',
    //   component: Dash,
    //   children: [
    //     { path: '', component: DataCard },
    //     { path: 'camera', component: Monitor },
    //     { path: 'settings', component: Settings }
    //   ]
    // }
    {
      path: '/',
      component: Dash,
    }
  ]
})
