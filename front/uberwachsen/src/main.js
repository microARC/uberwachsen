// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import VueSocketio from 'vue-socket.io'
import Vuetify from 'vuetify'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'vuetify/dist/vuetify.min.css'

Vue.use(VueSocketio, 'http://localhost:3000/');
Vue.use(Vuetify);

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  // sockets: {
  //   connect: function () {
  //     console.log('connect function');
  //   },
  //   'mqtt-msg': function (val) {
  //     console.log(val);
  //   }
  // }
})
